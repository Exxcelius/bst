package Structure;

public class NodeData {

    private String data;
    private int count = 1;

    public NodeData(String data){
        this.data = data;
    }

    public int compareAndCount(NodeData o){
        int value = this.compare(o);
        if(value == 0) count += 1;
        return value;
    }


    public int compare(NodeData o) {
        return data.compareTo(o.data);
    }

    @Override
    public String toString() {
        return String.format("%s:%d", data, count);
    }

    public void increment() {
        count++;
    }
}
