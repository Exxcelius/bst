package Structure;

import java.util.List;

public class Tree {
    Node root;

    public Tree(){
        root = null;
    }

    public static Tree buildTree(String input){
        input = input.toUpperCase();
        List<String> data = List.of(input.split(" "));

        Tree bst = new Tree();
        for (String item : data) {
            if (!item.equals("@")) {
                bst.insert(item);
            }
        }
        return bst;
    }

    private void insert(String data){
        root = findOrInsertRec(this.root, data);
    }

    public Node findOrInsert(String data) {
        return findOrInsertRec(root, data);
    }

    Node findOrInsertRec(Node root, String d) {
        if (root == null) {
            root = new Node(d);
            return root;
        }

        NodeData data = new NodeData(d);

        if (root.data.compare(data) < 0) {
            root.left = findOrInsertRec(root.left, d);
        } else if (root.data.compare(data) > 0) {
            root.right = findOrInsertRec(root.right, d);
        }
        else{
            root.increment();
        }
        return root;
    }

    public void preOrderTraversal(){
        preOrderTraversal(this.root);
    }

    public void inOrderTraversal(){
        inOrderTraversal(this.root);
    }

    public void postOrderTraversal(){
        postOrderTraversal(this.root);
    }



    private void preOrderTraversal(Node start){
        start.visit();
        if(start.left != null) preOrderTraversal(start.left);
        if(start.right != null) preOrderTraversal(start.right);
    }

    private void postOrderTraversal(Node start){
        if(start.left != null) postOrderTraversal(start.left);
        if(start.right != null) postOrderTraversal(start.right);
        start.visit();
    }

    private void inOrderTraversal(Node start){
        if(start.left != null) inOrderTraversal(start.left);
        start.visit();
        if(start.right != null) inOrderTraversal(start.right);
    }
}
