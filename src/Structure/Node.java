package Structure;

public class Node {
    public NodeData data;
    Node left, right;

    public Node(String data) {
        this.data = new NodeData(data);
    }

    public void visit(){
        System.out.print(data.toString() + " ");
    }

    public void increment() {
        this.data.increment();
    }
}
