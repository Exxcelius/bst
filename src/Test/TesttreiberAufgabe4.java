package Test;
import Structure.Tree;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class TesttreiberAufgabe4 {

    static String path = "C:\\Users\\nils-\\Documents\\Uni\\SemIV\\KP\\Buchholz\\BST\\words2count.txt";
    public static void main(String[] args) {
        Path filepath = Path.of(path);
        String content;
        try {
            content = Files.readString(filepath);
        } catch (IOException e) {
            System.err.println("Error occurred during file reading");
            return;
        }

        Tree bst = Tree.buildTree(content);

        bst.inOrderTraversal();



    }


}
