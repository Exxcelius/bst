package Test;

import Structure.Tree;

import java.util.Scanner;

public class TesttreiberAufgabe2 {
    public static void main(String[] args){
        String input, str1, str2;
        Tree inputTree, tree1, tree2;

        str1 = "A B @ @ C @ @";
        str2 = "A B @ C @ @";
        input = "5 2 1 0 @ @ @ 4 3 @ @ @ 8 6 @ 7 @ @ 9 @ X @ @";
        //input = new Scanner(System.in).next();

        tree1 = Tree.buildTree(str1);
        tree2 = Tree.buildTree(str2);
        inputTree = Tree.buildTree(input);

        tree1.preOrderTraversal();
        System.out.println("");
        tree2.inOrderTraversal();

        System.out.print("\nThe pre-order traversal is ");
        inputTree.preOrderTraversal();
        System.out.print("\nThe in-order traversal is ");
        inputTree.inOrderTraversal();
        System.out.print("\nThe post-order traversal is ");
        inputTree.postOrderTraversal();

    }
}
